package main

import (
	"fmt"
	"gitlab.com/backend-logging-library/logging-library/logger"
	"go.uber.org/zap"
)

func main() {
	logger.InitLogger("Asia/Makassar")

	uuid := logger.GenerateUUID()
	logger.LogInfo("test log", uuid)
	fmt.Println("=============================================")
	uuid = logger.GenerateUUID()
	logger.LogInfo("test log 2", uuid, zap.String("msg2", "test message lagi"))

}
