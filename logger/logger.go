package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type ZapField struct {
	Key   string
	Value any
}

func LogError(str any, uuid string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid))
	Logger.Error(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogErrorWithFunctionName(str any, uuid string, functionName string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("functionName", functionName))

	Logger.Error(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogErrorWithStackTrace(str any, uuid string, stackTrace string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("stackTrace", stackTrace))

	Logger.Error(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogErrorWithStackTraceAndFunctionName(str any, uuid string, functionName string, stackTrace string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("functionName", functionName), zap.String("stackTrace", stackTrace))

	Logger.Error(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogInfo(str any, uuid string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid))

	Logger.Info(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogInfoWithData(str any, uuid string, data any, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.Any("data", data))

	Logger.Info(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogInfoWithDataAndFunctionName(str any, uuid string, functionName string, data any, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("functionName", functionName), zap.Any("data", data))

	Logger.Info(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogInfoWithFunctionName(str any, uuid string, functionName string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("functionName", functionName))

	Logger.Info(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogWarn(str any, uuid string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid))

	Logger.Warn(fmt.Sprintf("%v", str),
		fields...,
	)
}

func LogWarnWithFunctionName(str any, uuid string, functionName string, fields ...zapcore.Field) {
	fields = append(fields, zap.String("uuid", uuid), zap.String("functionName", functionName))

	Logger.Warn(fmt.Sprintf("%v", str),
		fields...,
	)
}
